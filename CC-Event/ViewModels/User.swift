//
//  User.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/23.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

struct User {
    
    var id: Int!
    var name: String!
    var image: UIImage!
    var message: String
    
    init(id: Int, name: String, image: UIImage, message:String) {
        self.id = id
        self.name = name
        self.image = image
        self.message = message
    }
}
