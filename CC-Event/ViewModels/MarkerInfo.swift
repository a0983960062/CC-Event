//
//  MarkerInfo.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/4/25.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

struct MarkerInfo {

    var name: String!
    var image: UIImage!
    var lat: Double!
    var long: Double!
    var type: String!
    var status: String!
    var title: String!
    var startDate: String!
    var endDate: String!
    
    init(name: String, image: UIImage, lat: Double, long: Double, type: String, status: String, title: String, startDate: String, endDate: String) {
        self.name = name
        self.image = image
        self.lat = lat
        self.long = long
        self.type = type
        self.status = status
        self.title = title
        self.startDate = startDate
        self.endDate = endDate
    }
    
}

struct Event {
    
    var name: String!
    var image: UIImage!
    var lat: Double!
    var long: Double!
    var type: String!
    var status: String!
    var title: String!
    var startDate: String!
    var endDate: String!
    var likeStatu: Bool!
    
    init(name: String, image: UIImage, lat: Double, long: Double, type: String, status: String, title: String, startDate: String, endDate: String, likeStatu:Bool) {
        self.name = name
        self.image = image
        self.lat = lat
        self.long = long
        self.type = type
        self.status = status
        self.title = title
        self.startDate = startDate
        self.endDate = endDate
        self.likeStatu = likeStatu
    }
}
