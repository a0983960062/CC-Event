//
//  MapReuseTableViewCell.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/7.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class MapReuseTableViewCell: UITableViewCell {
    
    @IBOutlet var leftBtn: UIButton!
    @IBOutlet var rightBtn: UIButton!
    @IBOutlet var leftImg: UIImageView!
    @IBOutlet var rightImg: UIImageView!
    
    @IBOutlet var myImageView: UIImageView!
    @IBOutlet var catagory: UILabel!
    @IBOutlet var place: UILabel!
    @IBOutlet var statu: UILabel!
    @IBOutlet var title: UILabel!
    @IBOutlet var eventDate: UILabel!
    @IBOutlet var like: UIButton!
    
    @IBOutlet var constraint: NSLayoutConstraint!
    
    @IBOutlet var fakeContentView: UIView!
    
    @IBOutlet var friendImages: [UIImageView]!
    
    var images: [UIImage] = []
    
    var likeStatu: Bool = true
    
    func configCell(image: UIImage, catagory: String, place: String, status: String, title: String, startDate: String, endDate: String) {
        self.myImageView.image = image
        self.catagory.text = catagory
        self.place.text = place
        self.statu.text = status
        self.title.text = title
        self.eventDate.text = startDate + " ～ " + endDate
        
        if constraint.constant != 0 {
            constraint.constant = 0
            self.layoutIfNeeded()
        }
    }
    
    func setImage(images: [UIImage]) {
        self.images = images
    }
    
    @IBAction func tabLikeButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    
    override func layoutSubviews() {
        
//        self.contentView.layer.cornerRadius = 5
//        self.layer.shadowColor = UIColor.gray.cgColor
//        self.layer.shadowOffset = CGSize(width: 1, height: 2)
//        self.layer.shadowOpacity = 0.3
//        self.layer.shadowRadius = 5
//        self.layer.masksToBounds = false
//        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        print("\(images.count - 3)+")
        
        let lbltxt = "\(images.count - 3)+"
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        label.text = lbltxt
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        
        if images.count > 4 {
            friendImages[0].image = images[0]
            friendImages[1].image = images[1]
            friendImages[2].image = images[2]
            friendImages[3].addSubview(label)
        }
        
        switch images.count {
        case 0:
            return
        case 1:
            friendImages[3].image = images[0]
        case 2:
            friendImages[2].image = images[0]
            friendImages[3].image = images[1]
        case 3:
            friendImages[1].image = images[0]
            friendImages[2].image = images[1]
            friendImages[3].image = images[2]
        case 4:
            friendImages[0].image = images[0]
            friendImages[1].image = images[1]
            friendImages[2].image = images[2]
            friendImages[3].image = images[3]
        default:
            return
        }
        
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.fakeContentView.layer.cornerRadius = 5
        self.fakeContentView.layer.shadowColor = UIColor.gray.cgColor
        self.fakeContentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.fakeContentView.layer.shadowOpacity = 0.7
        self.fakeContentView.layer.shadowRadius = 2
        self.fakeContentView.layer.masksToBounds = false
//        self.fakeContentView.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panMyContentView))
        pan.delegate = self
        self.fakeContentView.addGestureRecognizer(pan)
        
        
        for image in friendImages {
            image.layer.cornerRadius = 15
            image.layer.masksToBounds = true
        }
        
        let unlikeImage = UIImage(named: "unlike")
        let likeImage = UIImage(named: "like")
        
        if likeStatu {
            like.setImage(likeImage, for: .selected)
        } else {
            like.setImage(unlikeImage, for: .normal)
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @objc func panMyContentView(sender:UIPanGestureRecognizer) {
        let point = sender.translation(in: self)
        constraint.constant += point.x
        
        
        if sender.state == .ended {
            if constraint.constant < -80 {
                constraint.constant = -80
            } else if constraint.constant > 80 {
                constraint.constant = 80
            } else if constraint.constant > -80 && constraint.constant < 80 {
                constraint.constant = 0
                
            }
            UIView.animate(withDuration: 0.25, animations: {
                self.layoutIfNeeded()
            })
        }else{
            self.layoutIfNeeded()
        }
        
        
        sender.setTranslation(.zero, in: self)
    }

}
