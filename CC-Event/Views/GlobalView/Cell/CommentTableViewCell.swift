//
//  CommentTableViewCell.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/23.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet var photo: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var comment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        photo.layer.cornerRadius = 30
        photo.layer.masksToBounds = true
        // Initialization code
    }
    
    func configCell(photo: UIImage, name: String, comment: String) {
        self.photo.image = photo
        self.name.text = name
        self.comment.text = comment
    }
    
    

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
