//                                 |~~~~~~~|
//                                 |       |
//                                 |       |
//                                 |       |
//                                 |       |
//                                 |       |
//      |~.\\\_\~~~~~~~~~~~~~~xx~~~         ~~~~~~~~~~~~~~~~~~~~~/_//;~|
//      |  \  o \_         ,XXXXX),                         _..-~ o /  |
//      |    ~~\  ~-.     XXXXX`)))),                 _.--~~   .-~~~   |
//       ~~~~~~~`\   ~\~~~XXX' _/ ';))     |~~~~~~..-~     _.-~ ~~~~~~~
//                `\   ~~--`_\~\, ;;;\)__.---.~~~      _.-~
//                  ~-.       `:;;/;; \          _..-~~
//                     ~-._      `''        /-~-~
//                         `\              /  /
//                           |         ,   | |
//                            |  '        /  |
//                             \/;          |
//                              ;;          |
//                              `;   .       |
//                              |~~~-----.....|
//                             | \             \
//                            | /\~~--...__    |
//                            (|  `\       __-\|
//                            ||    \_   /~    |
//                            |)     \~-'      |
//                             |      | \      '
//                             |      |  \    :
//                              \     |  |    |
//                               |    )  (    )
//                                \  /;  /\  |
//                                |    |/   |
//                                |    |   |
//                                 \  .'  ||
//                                 |  |  | |
//                                 (  | |  |
//                                 |   \ \ |
//                                 || o `.)|
//                                 |`\\\\) |
//                                 |       |
//                                 |       |
//    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//                      耶穌保佑                永無 BUG
//
//  EventDetailViewController.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/11.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class EventDetailViewController: UIViewController {
    
    let comment: [User] = [User(id: 1, name: "黑人", image: #imageLiteral(resourceName: "5628dd6ecd9fa100f371_size30_w521_h534"), message: "????????????"), User(id: 2, name: "西野七瀨", image: #imageLiteral(resourceName: "d1186073"), message: "おいしい"), User(id: 3, name: "于文文", image: #imageLiteral(resourceName: "RAHhaAJQZ7HNYNjbYuOoQGkCPhSGaey0gEt72F1ZLrMHW1515549845303"), message: "動手做好好玩")]
    
    let event: [Event] = [Event(name: "文化中心", image: #imageLiteral(resourceName: "img1"), lat: 22.626899, long: 120.317626, type: "音樂", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "高雄美術館", image: #imageLiteral(resourceName: "img2"), lat: 22.656457, long: 120.286030, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "衛武營藝術文化中心", image: #imageLiteral(resourceName: "img3"), lat: 22.621841, long: 120.339590, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "大東文化藝術中心", image: #imageLiteral(resourceName: "img4"), lat: 22.624882, long: 120.363455, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "高雄國家體育場", image: #imageLiteral(resourceName: "img5"), lat: 22.702927, long: 120.294758, type: "體育", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "駁二藝術特區", image: #imageLiteral(resourceName: "img6"), lat: 22.620091, long: 120.281495, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "國立科學工藝博物館", image: #imageLiteral(resourceName: "img7"), lat: 22.640504, long: 120.322435, type: "科學", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true)]
    
    @IBOutlet var signUpButton: UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var subView: UIView!
    @IBOutlet var joinPeople: UICollectionView!
    @IBOutlet var eventImage: UIImageView!
    @IBOutlet var type: UILabel!
    @IBOutlet var place: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var status: UILabel!
    
    @IBOutlet var suggestEvent: UICollectionView!
    
    let apperence = UINavigationBar.appearance()
    let backImage = UIImage(named: "back")
    var info: MarkerInfo?
    
    func setInfo(info: MarkerInfo) {
        self.info = info
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrollView.delegate = self
        
        self.suggestEvent.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        suggestEvent.register(UINib(nibName: "SuggestCell", bundle: nil), forCellWithReuseIdentifier: "SuggestEventCell")
        
        signUpButton.layer.cornerRadius = 15
        
        apperence.backIndicatorImage = backImage
        apperence.backIndicatorTransitionMaskImage = backImage
        apperence.tintColor = UIColor(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 1.0)
        configBarButtonItem()
        
        let gradient = CAGradientLayer()
        gradient.frame = subView.bounds
        gradient.frame.size.width = self.view.frame.width
        gradient.colors = [UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.0).cgColor, UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor]
        
        subView.layer.insertSublayer(gradient, at: 0)
        
        eventImage.image = info?.image
        type.text = info?.type
        place.text = info?.name
        name.text = info?.title
        date.text = (info?.startDate)! + "-" + (info?.endDate)!
        status.text = info?.status
        
        print(scrollView.frame.height)
        print(containerView.frame.height)
        print(scrollView.contentSize.height)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize.height = self.containerView.frame.height
        print(scrollView.contentSize.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    func configBarButtonItem() {
        let inviteFriend = UIImage(named: "inviteFriend")
        let shared = UIImage(named: "share")
        let unlike = UIImage(named: "unlike")
        let like = UIImage(named: "like")

        
        let inviteButton = UIButton(type: .custom)
        inviteButton.setImage(inviteFriend, for: UIControlState.normal)
        inviteButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let inviteBarButton = UIBarButtonItem(customView: inviteButton)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(shared, for: UIControlState.normal)
        shareButton.addTarget(self, action: #selector(didTapShareButton(sender:)), for: UIControlEvents.touchUpInside)
        shareButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let shareBarButton = UIBarButtonItem(customView: shareButton)
        
        let likeButton = UIButton(type: .custom)
        likeButton.setImage(unlike, for: UIControlState.normal)
        likeButton.setImage(like, for: UIControlState.selected)
        likeButton.addTarget(self, action: #selector(didTapLikeButton(sender:)), for: UIControlEvents.touchUpInside)
        likeButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        let likeBarButton = UIBarButtonItem(customView: likeButton)
        
        navigationItem.rightBarButtonItems = [likeBarButton, shareBarButton, inviteBarButton]
    }

}

extension EventDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count: Int!
        
        if collectionView == self.suggestEvent {
            count = event.count
        }
        
        return count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestEventCell", for: indexPath) as! SuggestEventCollectionViewCell
        
        cell.configCell(eventImage: event[indexPath.item].image, likeStatu: event[indexPath.item].likeStatu, eventStatu: event[indexPath.item].status, eventCatagory: event[indexPath.item].type, eventRegion: event[indexPath.item].name, eventName: event[indexPath.item].title)
        
        return cell
    }

}

extension EventDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        
        
        
        cell.configCell(photo: comment[indexPath.item].image, name: comment[indexPath.item].name, comment: comment[indexPath.item].message)
        
        return cell
    }
    
}

extension EventDetailViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            if scrollView.contentOffset == CGPoint(x: 0, y: 0) {
                self.navigationController?.navigationBar.isTranslucent = true
            } else {
                self.navigationController?.navigationBar.isTranslucent = false
            }
        }
        
        self.navigationController?.navigationBar.isHidden = false
    }
}
