//
//  LoginViewController.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/6/7.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn
import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    @IBAction func backTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    var profileImage: UIImage!
    var dict : [String : AnyObject]!
    
    func getProfilePicture(url: URL) {
        let session = URLSession(configuration: .default)
        
        let getImageFromUrl = session.dataTask(with: url) { (data, response, error) in
            
            if let e = error {
                print("Error Occurred: \(e)")
            } else {
                if (response as? HTTPURLResponse) != nil {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        self.profileImage = image
                    } else {
                        print("Image file is currupted")
                    }
                } else {
                    print("No response from server")
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.facebookLogin()
        self.googleLogin()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}

extension LoginViewController: FBSDKLoginButtonDelegate {
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        self.getFacebookLoginUserInfo(completion: { userInfo, error in
            if let error = error { print(error.localizedDescription)}
        })
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Logged out")
    }
    
    func facebookLogin() {
        
        let x = (self.view.frame.width - 200) / 2
        
        let facebookLoginButton = FBSDKLoginButton()
        view.addSubview(facebookLoginButton)
        if Device.IS_IPHONE_X {
            facebookLoginButton.frame = CGRect(x: x, y: 595, width: 200, height: 40)
        } else if Device.IS_IPHONE_6 {
            facebookLoginButton.frame = CGRect(x: x, y: 585, width: 200, height: 40)
        } else if Device.IS_IPHONE_5 {
            facebookLoginButton.frame = CGRect(x: x, y: 500, width: 200, height: 40)
        } else if Device.IS_IPHONE_6P {
            facebookLoginButton.frame = CGRect(x: x, y: 585, width: 200, height: 40)
        }
        
        facebookLoginButton.delegate = self
        facebookLoginButton.layer.shadowColor = UIColor.gray.cgColor
        facebookLoginButton.layer.shadowOpacity = 1.0
        facebookLoginButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        facebookLoginButton.readPermissions = ["email", "public_profile"]
        
        //        let loginManager = LoginManager()
        //
        //        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { result in
        //
        //            switch result {
        //            case .failed(let error):
        //                print(error.localizedDescription)
        //            case .cancelled:
        //                print("User cancelled the login")
        //            case .success(_,_,_):
        //                self.getFacebookLoginUserInfo{ userInfo, error in
        //                    if let error = error { print(error.localizedDescription)}
        //                }
        //            }
        //        }
    }
    
    func getFacebookLoginUserInfo(completion: @escaping (_ : [String: Any]?, _ : Error?) -> Void) {
        
        let request = GraphRequest(graphPath: "me", parameters: ["fields" : "id, name, email, picture"])
        
        request.start { response, result in
            switch result {
            case .failed(let error):
                completion(nil, error)
            case .success(let graphResponse):
                completion(graphResponse.dictionaryValue, nil)
                print(graphResponse.dictionaryValue!)
            }
        }
    }
    
}

extension LoginViewController: GIDSignInUIDelegate {
    
    
    
    func googleLogin() {
        
        var error: NSError?
        
        if error != nil {
            print(error)
            return
        }
        
        let googleSignInButton = GIDSignInButton()
        view.addSubview(googleSignInButton)
        
        //        let x: Int = Int((view.frame.width / 2) - (googleSignInButton.frame.width) + 20)
        
        let x = (self.view.frame.width - 200) / 2
        
        if Device.IS_IPHONE_X {
            googleSignInButton.frame = CGRect(x: x - 3, y: 520, width: 205, height: 30)
        } else if Device.IS_IPHONE_6 {
            googleSignInButton.frame = CGRect(x: x - 3, y: 520, width: 205, height: 30)
        } else if Device.IS_IPHONE_5 {
            googleSignInButton.frame = CGRect(x: x - 3, y: 500, width: 205, height: 30)
        } else if Device.IS_IPHONE_6P {
            googleSignInButton.frame = CGRect(x: x - 3, y: 520, width: 205, height: 30)
        }
        
        googleSignInButton.colorScheme = .light
        googleSignInButton.layer.shadowColor = UIColor.gray.cgColor
        googleSignInButton.layer.shadowOpacity = 0.3
        googleSignInButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        googleSignInButton.style = .wide
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil{
            print(error ?? "google error")
            return
        }
        print(user.profile.email)
        let url: URL = user.profile.imageURL(withDimension: 154)
        getProfilePicture(url: url)
        print(url)
        
    }
    
}
