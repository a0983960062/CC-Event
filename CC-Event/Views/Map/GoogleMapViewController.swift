//                                 |~~~~~~~|
//                                 |       |
//                                 |       |
//                                 |       |
//                                 |       |
//                                 |       |
//      |~.\\\_\~~~~~~~~~~~~~~xx~~~         ~~~~~~~~~~~~~~~~~~~~~/_//;~|
//      |  \  o \_         ,XXXXX),                         _..-~ o /  |
//      |    ~~\  ~-.     XXXXX`)))),                 _.--~~   .-~~~   |
//       ~~~~~~~`\   ~\~~~XXX' _/ ';))     |~~~~~~..-~     _.-~ ~~~~~~~
//                `\   ~~--`_\~\, ;;;\)__.---.~~~      _.-~
//                  ~-.       `:;;/;; \          _..-~~
//                     ~-._      `''        /-~-~
//                         `\              /  /
//                           |         ,   | |
//                            |  '        /  |
//                             \/;          |
//                              ;;          |
//                              `;   .       |
//                              |~~~-----.....|
//                             | \             \
//                            | /\~~--...__    |
//                            (|  `\       __-\|
//                            ||    \_   /~    |
//                            |)     \~-'      |
//                             |      | \      '
//                             |      |  \    :
//                              \     |  |    |
//                               |    )  (    )
//                                \  /;  /\  |
//                                |    |/   |
//                                |    |   |
//                                 \  .'  ||
//                                 |  |  | |
//                                 (  | |  |
//                                 |   \ \ |
//                                 || o `.)|
//                                 |`\\\\) |
//                                 |       |
//                                 |       |
//    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//                      耶穌保佑                永無 BUG
//
//  GoogleMapViewController.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/4/23.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

class POIItem: GMSMarker, GMUClusterItem {
    
    var name: String!
    
    init(name: String) {
        self.name = name
    }
    
}

class GoogleMapViewController: UIViewController, CLLocationManagerDelegate, GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate {
    
    @IBOutlet var googleMapContainer: UIView!
    @IBOutlet var infotableView: UITableView!
    
    let markers: [MarkerInfo] = [MarkerInfo(name: "文化中心", image: #imageLiteral(resourceName: "img1"), lat: 22.626899, long: 120.317626, type: "音樂", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28"), MarkerInfo(name: "高雄美術館", image: #imageLiteral(resourceName: "img2"), lat: 22.656457, long: 120.286030, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28"), MarkerInfo(name: "衛武營藝術文化中心", image: #imageLiteral(resourceName: "img3"), lat: 22.621841, long: 120.339590, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28"), MarkerInfo(name: "大東文化藝術中心", image: #imageLiteral(resourceName: "img4"), lat: 22.624882, long: 120.363455, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28"), MarkerInfo(name: "高雄國家體育場", image: #imageLiteral(resourceName: "img5"), lat: 22.702927, long: 120.294758, type: "體育", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28"), MarkerInfo(name: "駁二藝術特區", image: #imageLiteral(resourceName: "img6"), lat: 22.620091, long: 120.281495, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28"), MarkerInfo(name: "國立科學工藝博物館", image: #imageLiteral(resourceName: "img7"), lat: 22.640504, long: 120.322435, type: "科學", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28")]
    
    var locationManager: CLLocationManager!
    var infoView = UITableView()
    
    var mapView: GMSMapView!
    var clusterManager: GMUClusterManager!
    var globalMarker = GMSMarker()
    
    let isClustering: Bool = true
    let isCustom: Bool = true
    
    let customMarkerWidth: Int = 50
    let customMarkerHeight: Int = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
        
        var height: CGFloat!
        
        let backImage = UIImage(named: "back")
        let backButton = UIButton(type: .custom)
        backButton.setImage(backImage, for: .normal)
        backButton.frame = CGRect(x: 30, y: 0, width: 50, height: 50)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(customView: backButton)
        
        if Device.IS_IPHONE_X {
            height = self.googleMapContainer.frame.height / 5 * 3
        } else {
            height = self.googleMapContainer.frame.height / 2
        }
    
        let myLocationButton = UIButton(frame: CGRect(x: self.view.frame.width - 80, y: height - 80, width: 60, height: 60))
        myLocationButton.layer.cornerRadius = 30
        myLocationButton.backgroundColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        myLocationButton.setImage(UIImage(named: "navi"), for: UIControlState.normal)
        myLocationButton.addTarget(self, action: #selector(gotoMyLocation(sender:)), for: .touchUpInside)
        
//        print(locationManager.location?.coordinate.latitude)
        
        let lat = (locationManager.location?.coordinate.latitude)!
        let lng = (locationManager.location?.coordinate.longitude)!
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 15)
        
        mapView = GMSMapView.map(withFrame: CGRect(x:0, y:0, width: self.view.frame.width, height: height), camera: camera)
        
        self.mapView.addSubview(myLocationButton)
        
        if isClustering {
            var iconGenerator: GMUDefaultClusterIconGenerator!
            if isCustom { // Here's my image if the event are clustered
                iconGenerator = GMUDefaultClusterIconGenerator(buckets: [100], backgroundColors: [UIColor(red: 255.0/255.0, green: 98.0/255.0, blue: 98.0/255.0, alpha: 1.0)])
            } else {
                iconGenerator = GMUDefaultClusterIconGenerator()
            }
            
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
            
            clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
            clusterManager.cluster()
            clusterManager.setDelegate(self, mapDelegate: self)
            renderer.delegate = self
        } else {
        }

        self.infoView = infotableView
        self.infoView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        infoView.register(UINib(nibName: "EventTableCell", bundle: nil), forCellReuseIdentifier: "EventTableCell")
        
        settingMap()
        
        clusterManager.setDelegate(self, mapDelegate: self)
        
        generateClusterItems()
        
        clusterManager.cluster()
        
        setupScrollView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.settingMap()
        self.locationManager.stopUpdatingLocation()
    }
    
    func generateClusterItems() {
        for index in markers {
            let item = POIItem(name: index.name)
            item.position = CLLocationCoordinate2DMake(index.lat, index.long)
            clusterManager.add(item)
        }
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData is POIItem {
            marker.icon = UIImage(named: "Marker")
        }
    }
    
    func setupScrollView() {
        
        self.view.addSubview(infoView)
        
        infoView.translatesAutoresizingMaskIntoConstraints = false
        let scrollBottomConstraint = NSLayoutConstraint(item: infoView, attribute: .bottom, relatedBy: .equal, toItem: googleMapContainer, attribute: .bottomMargin, multiplier: 1, constant: 0)
        let scrollTopConstraint = NSLayoutConstraint(item: infoView, attribute: .top, relatedBy: .equal, toItem: self.mapView, attribute: .bottom, multiplier: 1, constant: 0)
        let scrollLeadingConstriant = NSLayoutConstraint(item: infoView, attribute: .leading, relatedBy: .equal, toItem: googleMapContainer, attribute: .leading, multiplier: 1, constant: 0)
        let scrollTrailingConstriant = NSLayoutConstraint(item: infoView, attribute: .trailing, relatedBy: .equal, toItem: googleMapContainer, attribute: .trailing, multiplier: 1, constant: 0)
        view.addConstraints([scrollBottomConstraint, scrollTopConstraint, scrollLeadingConstriant, scrollTrailingConstriant])
        infoView.frame.size.height = self.view.frame.height - self.googleMapContainer.frame.height
        
//        let title = UILabel()
//        title.textColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)
//        title.text = "走路就到"
//        title.font = UIFont.boldSystemFont(ofSize: 20.0)
//        infoView.addSubview(title)
//
//        title.translatesAutoresizingMaskIntoConstraints = false
//        let titleTopConstraint = NSLayoutConstraint(item: title, attribute: .top, relatedBy: .equal, toItem: infoView, attribute: .top, multiplier: 1, constant: 16)
//        let titleLeadingConstriant = NSLayoutConstraint(item: title, attribute: .leading, relatedBy: .equal, toItem: infoView, attribute: .leadingMargin, multiplier: 1, constant: 16)
//        let titleTrailingConstraint = NSLayoutConstraint(item: title, attribute: .trailing, relatedBy: .equal, toItem: infoView, attribute: .trailingMargin, multiplier: 1, constant: 0)
//        title.frame.size.height = 28
//        infoView.addConstraints([titleTopConstraint, titleLeadingConstriant, titleTrailingConstraint])
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        globalMarker = marker
        if let poiItem = marker.userData as? POIItem {
            marker.title = poiItem.name
            marker.snippet = "\(poiItem.position.latitude)\n\(poiItem.position.longitude)"
            NSLog("Did tap marker for cluster item \(poiItem.name!)")
        } else {
            NSLog("Did tap a normal marker")
        }
        return false
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if globalMarker.userData != nil {
            
        }
    }
    
//    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        let height = self.view.frame.height
//        let viewHeight = self.view.frame.height / 3
//        let subView = UIView(frame: CGRect(x: 32, y: height - viewHeight, width: self.view.frame.width - 64, height: viewHeight))
//        subView.backgroundColor = UIColor.white.withAlphaComponent(0.7)
//        return subView
//    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let camera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(camera)
        mapView.moveCamera(update)
        return false
    }
    
    func settingMap() {
        
//        let latitude = NSString(format: "%.5f", (self.locationManager.location?.coordinate.latitude)!)
//        let longitude = NSString(format: "%.5f", (self.locationManager.location?.coordinate.longitude)!)
        
//        self.latitudeLabel.text = latitude as String
//        self.longitudeLabel.text = longitude as String
        
        
//        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
//        for i in markers {
//            let marker = GMSMarker()
//            let position = CLLocationCoordinate2D(latitude: i.lat, longitude: i.long)
//            marker.position = position
//            marker.snippet = i.name
//
//            let customMarker = MarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: i.image, borderColor: UIColor.darkGray, tag: 1)
//            marker.iconView = customMarker
//            marker.appearAnimation = .pop
//            marker.map = mapView
//        }
        
        self.googleMapContainer.addSubview(mapView)
    }
    
    @objc func gotoMyLocation(sender: UIButton!) {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 15)
        mapView.animate(to: camera)
    }

}

extension GoogleMapViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return markers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableCell", for: indexPath) as! MapReuseTableViewCell
        
        cell.configCell(image: markers[indexPath.item].image, catagory: markers[indexPath.item].type, place: markers[indexPath.item].name, status: markers[indexPath.item].status, title: markers[indexPath.item].title, startDate: markers[indexPath.item].startDate, endDate: markers[indexPath.item].endDate)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: StoryboardID.main, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.eventDetail) as! EventDetailViewController
        
        viewController.setInfo(info: markers[indexPath.item])
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

