//
//  GoogleMap2ViewController.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/4/27.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class POIItem: NSObject, GMUClusterItem {
    
    var position: CLLocationCoordinate2D
    var name: String!
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
    
}

class GoogleMap2ViewController: UIViewController, GMUClusterManagerDelegate, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    let markers: [MarkerInfo] = [MarkerInfo(name: "文化中心", image: #imageLiteral(resourceName: "img1"), lat: 22.626899, long: 120.317626), MarkerInfo(name: "高雄美術館", image: #imageLiteral(resourceName: "img2"), lat: 22.656457, long: 120.286030), MarkerInfo(name: "衛武營藝術文化中心", image: #imageLiteral(resourceName: "img3"), lat: 22.621841, long: 120.339590), MarkerInfo(name: "大東文化藝術中心", image: #imageLiteral(resourceName: "img4"), lat: 22.624882, long: 120.363455), MarkerInfo(name: "高雄國家體育場", image: #imageLiteral(resourceName: "img5"), lat: 22.702927, long: 120.294758), MarkerInfo(name: "駁二藝術特區", image: #imageLiteral(resourceName: "img6"), lat: 22.620091, long: 120.281495), MarkerInfo(name: "國立科學工藝博物館", image: #imageLiteral(resourceName: "img7"), lat: 22.640504, long: 120.322435)]
    
    private var mapView: GMSMapView!
    private var clusterManager: GMUClusterManager!
    
    let locationManager = CLLocationManager()
    
    override func loadView() {
        let camera = GMSCameraPosition.camera(withLatitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!, zoom: 15)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.view = mapView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        iconGenerator.icon(forSize: 20)
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        
        setupMap()
        
        generateClusterItems()
        
        clusterManager.cluster()
        
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    func setupMap() {
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: Style.json)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let camera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(camera)
        mapView.moveCamera(update)
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            NSLog("Did tap marker for cluster item \(poiItem.name)")
        } else {
            NSLog("Did tap a normal marker")
        }
        return false
    }

    private func generateClusterItems() {
        for index in markers {
            let item = POIItem(position: CLLocationCoordinate2DMake(index.lat, index.long), name: index.name)
            clusterManager.add(item)
        }
    }
    
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }

}
