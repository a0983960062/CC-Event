//
//  AllPlanningViewController.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/6/7.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class AllPlanningViewController: UIViewController {
    
    var event: [Event] = [Event(name: "文化中心", image: #imageLiteral(resourceName: "img1"), lat: 22.626899, long: 120.317626, type: "音樂", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "高雄美術館", image: #imageLiteral(resourceName: "img2"), lat: 22.656457, long: 120.286030, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "衛武營藝術文化中心", image: #imageLiteral(resourceName: "img3"), lat: 22.621841, long: 120.339590, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "大東文化藝術中心", image: #imageLiteral(resourceName: "img4"), lat: 22.624882, long: 120.363455, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "高雄國家體育場", image: #imageLiteral(resourceName: "img5"), lat: 22.702927, long: 120.294758, type: "體育", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "駁二藝術特區", image: #imageLiteral(resourceName: "img6"), lat: 22.620091, long: 120.281495, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "國立科學工藝博物館", image: #imageLiteral(resourceName: "img7"), lat: 22.640504, long: 120.322435, type: "科學", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true)]
    
    let images: [UIImage] = [#imageLiteral(resourceName: "5628dd6ecd9fa100f371_size30_w521_h534"), #imageLiteral(resourceName: "RAHhaAJQZ7HNYNjbYuOoQGkCPhSGaey0gEt72F1ZLrMHW1515549845303"), #imageLiteral(resourceName: "d1186073"), #imageLiteral(resourceName: "7895504336403"), #imageLiteral(resourceName: "images")]
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "PlanningPrototype", bundle: nil), forCellReuseIdentifier: "PlanningTableCell")
        tableView.separatorStyle = .none
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        // Do any additional setup after loading the view.
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
}

extension AllPlanningViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return event.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlanningTableCell", for: indexPath) as! EventPrototypeTableViewCell
        
        cell.setImage(images: images)
        
        cell.configCell(image: event[indexPath.item].image, catagory: event[indexPath.item].type, place: event[indexPath.item].name, status: event[indexPath.item].status, title: event[indexPath.item].title, startDate: event[indexPath.item].startDate, endDate: event[indexPath.item].endDate)
        
        return cell
    }
    
}
