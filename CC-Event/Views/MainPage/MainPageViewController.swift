//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//               佛祖保佑         永無bug
//
//***************************************************
//
//  MainPageViewController.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/8.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit
import Social

class MainPageViewController: UIViewController {
    
    let photos = [#imageLiteral(resourceName: "ad001"), #imageLiteral(resourceName: "ad002"), #imageLiteral(resourceName: "ad003"), #imageLiteral(resourceName: "ad004"), #imageLiteral(resourceName: "ad005"), #imageLiteral(resourceName: "ad006")]
    
    let event: [Event] = [Event(name: "文化中心", image: #imageLiteral(resourceName: "img1"), lat: 22.626899, long: 120.317626, type: "音樂", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "高雄美術館", image: #imageLiteral(resourceName: "img2"), lat: 22.656457, long: 120.286030, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "衛武營藝術文化中心", image: #imageLiteral(resourceName: "img3"), lat: 22.621841, long: 120.339590, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "大東文化藝術中心", image: #imageLiteral(resourceName: "img4"), lat: 22.624882, long: 120.363455, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true), Event(name: "高雄國家體育場", image: #imageLiteral(resourceName: "img5"), lat: 22.702927, long: 120.294758, type: "體育", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "駁二藝術特區", image: #imageLiteral(resourceName: "img6"), lat: 22.620091, long: 120.281495, type: "藝術", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: false), Event(name: "國立科學工藝博物館", image: #imageLiteral(resourceName: "img7"), lat: 22.640504, long: 120.322435, type: "科學", status: "即將開始", title: "夏日水果冰自己動手做", startDate: "4月20", endDate: "4月28", likeStatu: true)]
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var goNowCollectionView: UICollectionView!
    @IBOutlet var goTonightCollectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet var goNowDetail: UIButton!
    @IBOutlet var goTonightDetail: UIButton!
    
    @IBAction func tabGoNowDetail(_ sender: Any) {
        let storyboard = UIStoryboard(name: StoryboardID.mainPage, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.allEvent)
        viewController.navigationItem.title = "現在就去"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func tapGoTonightDetail(_ sender: Any) {
        let storyboard = UIStoryboard(name: StoryboardID.mainPage, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.allEvent)
        viewController.navigationItem.title = "今晚就去"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func tapPlanning(_ sender: Any) {
        let storyboard = UIStoryboard(name: StoryboardID.mainPage, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.planning)
        viewController.navigationItem.title = "企劃樣式"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func tapGoLogin(_ sender: Any) {
        let storyboard = UIStoryboard(name: StoryboardID.main, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: StoryboardIdentifier.login)
        viewController.navigationItem.title = "會員登入"
        viewController.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0)

        present(viewController, animated: true, completion: nil)
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.pageControl.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        goNowCollectionView.register(UINib(nibName: "SuggestCell", bundle: nil), forCellWithReuseIdentifier: "SuggestEventCell")
        goTonightCollectionView.register(UINib(nibName: "SuggestCell", bundle: nil), forCellWithReuseIdentifier: "SuggestEventCell")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.handleTap))
        self.view.addGestureRecognizer(tap)
            
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setSearchBar()
        
        goNowDetail.layer.borderColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        goNowDetail.layer.borderWidth = 1
        goNowDetail.layer.cornerRadius = 11
        goNowDetail.setTitle("\(event.count)>", for: .normal)
        
        goTonightDetail.layer.borderColor = UIColor(red: 133.0/255.0, green: 183.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        goTonightDetail.layer.borderWidth = 1
        goTonightDetail.layer.cornerRadius = 11
        goTonightDetail.setTitle("\(event.count)>", for: .normal)
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        goNowCollectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        goTonightCollectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize.height = self.containerView.frame.height
        print(scrollView.contentSize.height)
        self.navigationController?.navigationBar.isTranslucent = false
        
        print(navigationController?.navigationBar.frame.height)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchController.resignFirstResponder()
    }
}

extension MainPageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionView {
            var width: Int!
            var height: Int!
            
            if Device.IS_IPHONE_5 {
                width = 290
                height = 163
            } else if Device.IS_IPHONE_6P {
                width = 384
                height = 216
            } else {
                width = 345
                height = 194
            }
            
            return CGSize(width: width, height: height)
        } else {
            return CGSize(width: 150, height: 200)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView {
            let count = photos.count
        
            self.pageControl.numberOfPages = count
            self.pageControl.isHidden = !(count > 1)
        
            return count
        } else if collectionView == goNowCollectionView {
            return event.count
        } else {
            return event.count
        }
    }
    
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  "MainPageCollectionViewCell", for: indexPath) as!    MainPageCollectionViewCell
        
            cell.contentView.frame.size = cell.frame.size
            cell.configCell(image: photos[indexPath.item])
        
            return cell
        } else if collectionView == goNowCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestEventCell", for: indexPath) as! SuggestEventCollectionViewCell
            
            cell.configCell(eventImage: event[indexPath.item].image, likeStatu: event[indexPath.item].likeStatu, eventStatu: event[indexPath.item].status, eventCatagory: event[indexPath.item].type, eventRegion: event[indexPath.item].name, eventName: event[indexPath.item].title)
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestEventCell", for: indexPath) as! SuggestEventCollectionViewCell
            
            cell.configCell(eventImage: event[indexPath.item].image, likeStatu: event[indexPath.item].likeStatu, eventStatu: event[indexPath.item].status, eventCatagory: event[indexPath.item].type, eventRegion: event[indexPath.item].name, eventName: event[indexPath.item].title)
            
            return cell
        }
    }

}

extension MainPageViewController: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == self.collectionView {
            
            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            var width: CGFloat!
            
            if Device.IS_IPHONE_5 {
                width = 290
            } else if Device.IS_IPHONE_6P {
                width = 384
            } else {
                width = 345
            }
            
            let cellWidthIncludingSpacing = width + layout.minimumLineSpacing
            
            var offset = targetContentOffset.pointee
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)
            
            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
            targetContentOffset.pointee = offset
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.collectionView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
        }
    }
    
}

extension MainPageViewController: UISearchControllerDelegate, UISearchBarDelegate {
    
    func setSearchBar() {
        
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.showsCancelButton = false
        navigationItem.largeTitleDisplayMode = .never
        //        searchController.searchBar.layer.borderWidth = 2
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.borderWidth = 2
                backgroundview.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0).cgColor
                backgroundview.layer.cornerRadius = 20
                backgroundview.clipsToBounds = true
            }
            
        }
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: " 快 來 找 活 動", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        //        searchController.searchBar.layer.borderColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0).cgColor
        //        searchController.searchBar.layer.cornerRadius = 20
        //        searchBar.placeholder = "快來找活動"
        //        searchBar.showsCancelButton = false
        //        searchBar.searchBarStyle = .minimal
        //        searchBar.delegate = self
        
        //        self.navigationItem.titleView = searchBar
        searchController.searchBar.setImage(UIImage(named: "search"), for: UISearchBarIcon.search, state: .normal)
        searchController.searchBar.tintColor = UIColor.black
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "取消"
        
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        searchController.hidesNavigationBarDuringPresentation = false
        
        let frame = CGRect(x: 0, y: 0, width: 300, height: 40)
        let title = UIView(frame: frame)
        searchController.searchBar.frame = frame
        title.addSubview(searchController.searchBar)
        navigationItem.titleView = title
        
        
        
        self.navigationItem.titleView = searchController.searchBar
        
        
        
//        navigationItem.searchController = searchController
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
//        self.searchController.searchBar.isTranslucent = false
        self.navigationController!.navigationBar.isTranslucent = false
        searchController.searchResultsController?.view.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchController.searchResultsController?.view.isHidden = true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        
        
    }
}

extension UIViewController {
    
    @objc public func didTapLikeButton(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        
    }
    
    @objc public func didTapShareButton(sender: AnyObject) {
        let URL = "www.google.com"
        let activityViewController = UIActivityViewController(activityItems: [URL], applicationActivities: [])
        activityViewController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        self.present(activityViewController, animated: true, completion: nil)
    }
}
