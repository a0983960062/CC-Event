//
//  MainPageCollectionViewCell.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/8.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class MainPageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        
        
        print(self.frame.size)
        print(self.imageView.frame.size)
    }
    
    override func layoutSubviews() {
        self.contentView.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func configCell(image: UIImage) {
        self.imageView.image = image
    }
    
}
