//
//  GoTonightCollectionViewCell.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/5/27.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import UIKit

class GoTonightCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var like: UIButton!
    @IBOutlet var statu: UILabel!
    @IBOutlet var region: UILabel!
    @IBOutlet var catagory: UILabel!
    @IBOutlet var name: UILabel!
    
    var likeStatu: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let unlikeImage = UIImage(named: "unlike")
        let likeImage = UIImage(named: "like")
        
        if likeStatu {
            like.setImage(likeImage, for: .selected)
        } else {
            like.setImage(unlikeImage, for: .normal)
        }
        
    }
    
    @IBAction func tabLikeButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func configCell(eventImage: UIImage, likeStatu: Bool, eventStatu: String, eventCatagory: String, eventRegion: String, eventName: String) {
        self.image.image = eventImage
        self.likeStatu = likeStatu
        self.statu.text = eventStatu
        self.catagory.text = eventCatagory
        self.region.text = eventRegion
        self.name.text = eventName
    }
}
