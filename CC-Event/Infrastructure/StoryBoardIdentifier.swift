//
//  StoryBoardIdentifier.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/4/23.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import Foundation

struct StoryboardIdentifier {
    static let map = "GoogleMapViewController"
    static let mainPage = "MainPageViewController"
    static let eventDetail = "EventDetailViewController"
    static let allEvent = "AllSuggestEventViewController"
    static let planning = "AllPlanningViewController"
    static let login = "LoginViewController"
}
