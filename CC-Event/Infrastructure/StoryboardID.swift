//
//  StoryboardID.swift
//  CC-Event
//
//  Created by Charles Chiang on 2018/4/23.
//  Copyright © 2018 Charles Chiang. All rights reserved.
//

import Foundation

struct StoryboardID {
    static let main = "Main"
    static let map = "Map"
    static let mainPage = "MainPage"
    static let friend = "Friend"
    static let calendar = "Calendar"
    static let member = "Member"
}
